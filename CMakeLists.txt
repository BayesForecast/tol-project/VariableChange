cmake_minimum_required (VERSION 2.8)

project (VariableChange)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/../cmake/modules")

add_subdirectory(CppTools)

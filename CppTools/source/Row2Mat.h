/* Row2Mat.h: Methods to convert from row to matrix

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#ifndef _DEFINE_Row2Mat_
#define _DEFINE_Row2Mat_

#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bmfstpro.h>
#include <tol/tol_bvmatgra.h>
#include <tol/tol_bpolgra.h>

bool Row2DiagonalV(const DMat& X, int row, BVMat& D);
bool ReverseRow2DiagonalV(const DMat& X, int row, BVMat& D);
bool Row2Square(const DMat& X, int row, DMat& M);
bool Row2SquareV(const DMat& X, int row, BVMat& M);
bool Row2LowTriang(const DMat& X, int row, DMat& M);
bool Row2LowTriangV(const DMat& X, int row, BVMat& M);


#endif

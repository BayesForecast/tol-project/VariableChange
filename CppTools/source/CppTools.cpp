/* CppTools.cpp: C++ API to invoke GSL from TOL

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */


//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_
#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bdatgra.h>
#include <tol/tol_bsetgra.h>
#include <tol/tol_bnameblock.h>
#include "Row2Mat.h"
#include "ChgVar.h"

#define dMat(arg) ((DMat&)Mat(arg))

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function
DynAPI void* GetDynLibNameBlockVariableChange()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatVector2LowTriangular);
DefMethod(1, BMatVector2LowTriangular, "Vector2LowTriangular", 1, 1, 
"Matrix",
"(Matrix x)",
"Builds a kxk low triangular matrix 'L' from a compact vector representation "
"'x' with k*(k+1)/2 cells corresponding to concatenated semi-rows of the "
"kxk matrix 'L'",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatVector2LowTriangular::CalcContens()
//--------------------------------------------------------------------
{
  DMat& L = *(DMat*)(&contens_);
  const DMat&  X = dMat(Arg(1));
  Row2LowTriang(X,0,L);
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatVector2Square);
DefMethod(1, BMatVector2Square, "Vector2Square", 1, 1, 
"Matrix",
"(Matrix x)",
"Builds a kxk square matrix 'M' from a compact vector representation "
"'x' with k*k cells corresponding to concatenated rows of the "
"kxk matrix 'M'",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatVector2Square::CalcContens()
//--------------------------------------------------------------------
{
  const DMat&  X = dMat(Arg(1));
  DMat& M = *(DMat*)(&contens_);
  Row2Square(X,0,M);
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatChgVarInsideHypersphere);
DefMethod(1, BMatChgVarInsideHypersphere, "ChgVar.InsideHypersphere", 1, 4, 
"Matrix Real Real Real",
"(Matrix x [, Real power=1, Real a=0, Real b=1])",
"Builds the hyperspherical change of variable"
"It returns a row of n columns with a point inside the hypersphere "
"defined in each row of 'x' in function "
"of radius 'r' stored in the first column and n-1 angles stored at "
"columns 2 to n of each row of 'x'.\n"
"Radius 'r' must be a positive real number in [0,1].\n"
"First n-2 angles in 'fi' must be in [0,Pi] and last angle in [0,2*Pi]\n"
"More details in sections "
"'Parameters sorted by signed value' and "
"'Parameters sorted by absolute value' of paper "
"'Changes of variables in black-box estimation methods' published at "
"https://www.tol-project.org/export/HEAD/tolp/OfficialTolArchiveNetwork/"
"VariableChange/doc/Changes%20of%20variables%20in%20MCMC%20methods/"
"Changes%20of%20variables%20in%20MCMC%20methods.pdf",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatChgVarInsideHypersphere::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[InsideHypersphere] step 1"));
  DMat& phi = *(DMat*)(&contens_);
  const DMat&  x = dMat(Arg(1));
  ChgVarInsideHypersphere(x,phi);
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatChgVarSortedBounded);
DefMethod(1, BMatChgVarSortedBounded, "ChgVar.SortedBounded", 1, 4, 
"Matrix Real Real Real",
"(Matrix x [, Real power=1, Real a=0, Real b=1])",
"Builds the hyperspherical change of variable to handle with parameters "
"bounded in interval [a,b] and that are sorted in function of value "
"of power=p:\n"
"  a^p <= lambda_1^p <= lambda_2^p <= ... <= lambda_n^p <= b^p\n"
"Although 'power' could be any integer number p, there are only four "
"distinct orders:\n"
" p=+1:  a   <=  lambda_1   <=  lambda_2   <= ... <=  lambda_n   <= b "
" p=+2:  a^2 <=  lambda_1^2 <=  lambda_2^2 <= ... <=  lambda_n^2 <= b^2 "
" p=-1:  a   >=  lambda_1   >=  lambda_2   >= ... >=  lambda_n   >= a "
" p=-2:  b^2 >=  lambda_1^2 >=  lambda_2^2 >= ... >=  lambda_n^2 >= a^2 "
"It returns a row of n columns 'lambda' for each row of 'x' in function "
"of radius 'r' stored in the first column and n-1 angles stored at "
"columns 2 to n of each row of 'x'.\n"
"Radius 'r' must be a positive real number in [0,1].\n"
"First n-2 angles in 'fi' must be in [0,Pi] and last angle in [0,2*Pi]\n"
"More details in sections "
"'Parameters sorted by signed value' and "
"'Parameters sorted by absolute value' of paper "
"'Changes of variables in black-box estimation methods' published at "
"https://www.tol-project.org/export/HEAD/tolp/OfficialTolArchiveNetwork/"
"VariableChange/doc/Changes%20of%20variables%20in%20MCMC%20methods/"
"Changes%20of%20variables%20in%20MCMC%20methods.pdf",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatChgVarSortedBounded::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[SortedBounded] step 1"));
  DMat& lambda = *(DMat*)(&contens_);
  const DMat&  x = dMat(Arg(1));
  double p = 1;
  double lower = 0;
  double upper = 1;
  if(Arg(2)) { p=Real(Arg(2)); }
  if(p>0)
  {
    if(Arg(3)) { lower = pow(Real(Arg(3)),p); }
    if(Arg(4)) { upper = pow(Real(Arg(4)),p); }
  }
  else
  {
    if(Arg(3)) { upper = pow(Real(Arg(3)),p); }
    if(Arg(4)) { lower = pow(Real(Arg(4)),p); }
  }  
  ChgVarSortedBounded(x,p,lower,upper,lambda);
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatChgVarCorrelationCholesky);
DefMethod(1, BMatChgVarCorrelationCholesky, "ChgVar.CorrelationCholesky", 1, 1, 
"Matrix",
"(Matrix x)",
"Builds the hyperspherical changes of variable to handle with k*(k-1)/2 "
"independently bounded parameters of a set of kxk correlations matrix 'rho'.\n"
"Each row in 'x' stores k*(k-1)/2 angles that has been compacted by rows, "
"and this function returns a row of k*(k-1)/2 Cholesky low triangular "
"decomposition 'W' of correlations 'rho'. Matrices 'W' also will be "
"compacted by rows in just one row of k*(k+1)/2 columns for each row "
"in 'x'.\n"
"Angles under first sub-diagonal in uncompacted 'x' in [0,Pi] and angles "
"in first sub-diagonal will be in [0,2*Pi]\n"
"Each returned row can be converted in a kxk low triangular matrix by "
"means of Vector2LowTriangular.\n"
"More details in section 'Correlations' of paper "
"'Changes of variables in black-box estimation methods' published at "
"https://www.tol-project.org/export/HEAD/tolp/OfficialTolArchiveNetwork/"
"VariableChange/doc/Changes%20of%20variables%20in%20MCMC%20methods/"
"Changes%20of%20variables%20in%20MCMC%20methods.pdf",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatChgVarCorrelationCholesky::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[Correlation] step 1"));
  DMat& W = *(DMat*)(&contens_);
  const DMat&  X = dMat(Arg(1));
  ChgVarCorrelation(X,W);
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatChgVarRotation);
DefMethod(1, BMatChgVarRotation, "ChgVar.Rotation", 1, 1, 
"Matrix",
"(Matrix x)",
"Builds the natural sorted composition of kxk Givens rotations\n"
"  G(theta,i,j) for i<j \n"
"Each row in 'x' stores k*(k-1)/2 angles in interval [0,2*Pi] that has "
"been compacted by rows, and this function returns a row of k*k cells "
"of composited rotation 'R' which also will be compacted by rows in "
"just one row of k*k columns for each row in 'x'.\n"
"Each returned row can be converted in a kxk squared matrix by means "
"of Vector2Square.\n"
"More details in section 'Orthogonal and Rotation Matrices' of paper "
"'Changes of variables in black-box estimation methods' published at "
"https://www.tol-project.org/export/HEAD/tolp/OfficialTolArchiveNetwork/"
"VariableChange/doc/Changes%20of%20variables%20in%20MCMC%20methods/"
"Changes%20of%20variables%20in%20MCMC%20methods.pdf",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatChgVarRotation::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[Correlation] step 1"));
  const DMat&  X = dMat(Arg(1));
  DMat& R = *(DMat*)(&contens_);
  ChgVarRotation(X, R);
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatChgVarRotatedDiagDecomSquare);
DefMethod(1, BMatChgVarRotatedDiagDecomSquare, "ChgVar.RotatedDiagDecom.Square", 1, 2, 
"Matrix Real",
"(Matrix x [, Real sortedEigenvalues=False])",
"Builds the kxk square matrix C which rotated diagonal decomposition is given by:\n"
"  C = R D Q \n"
"where R and Q are natural sorted Givens composited rotations and D is a "
"diagonal matrix\n"
"Each row in 'x' stores k*k cells representing such one decomposition where:\n "
" * first k cell are entries of diagonal in D\n"
" * next k*(k-1)/2 are angles of Givens rotations of R\n"
" * last k*(k-1)/2 are angles of Givens rotations of Q\n"
"It returns a kxk square matrix which also will be "
"compacted by rows in just one row of k*k columns for each row in 'x'.\n"
"Each returned row can be converted in a kxk squared matrix by means "
"of Vector2Square.\n"
"More details in section 'Non singular square matrices' of paper "
"'Changes of variables in black-box estimation methods' published at "
"https://www.tol-project.org/export/HEAD/tolp/OfficialTolArchiveNetwork/"
"VariableChange/doc/Changes%20of%20variables%20in%20MCMC%20methods/"
"Changes%20of%20variables%20in%20MCMC%20methods.pdf",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatChgVarRotatedDiagDecomSquare::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[Correlation] step 1"));
  DMat& C = *(DMat*)(&contens_);
  const DMat&  X = dMat(Arg(1));
  bool sortedEigenvalues = false;
  if(Arg(2)) { sortedEigenvalues = (bool)Real(Arg(2)); }
  ChgVarRotatedDiagDecomSquare(X,sortedEigenvalues,C);
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatChgVarStationaryPolyn);
DefMethod(1, BMatChgVarStationaryPolyn, "ChgVar.StationaryPolyn", 1, 1, 
"Matrix",
"(Matrix x)",
"Builds the stationary polynomial corresponding to recursive Duffin "
"coefficientes.\n"
"More details in section 'Stationary polynomials' of paper "
"'Changes of variables in black-box estimation methods' published at "
"https://www.tol-project.org/export/HEAD/tolp/OfficialTolArchiveNetwork/"
"VariableChange/doc/Changes%20of%20variables%20in%20MCMC%20methods/"
"Changes%20of%20variables%20in%20MCMC%20methods.pdf",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatChgVarStationaryPolyn::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[Correlation] step 1"));
  DMat& S = *(DMat*)(&contens_);
  const DMat&  X = dMat(Arg(1));
  ChgVarStationaryPolyn(X,S);
}

/* */
//--------------------------------------------------------------------
DeclareContensClass(BPolMat, BPolMatTemporary, BPolMatChgVarStationaryMatrixPolyn);
DefMethod(1, BPolMatChgVarStationaryMatrixPolyn, "ChgVar.StationaryMatrixPolyn", 2, 2, 
"Matrix Real",
"(Matrix x, Real degree)",
"Builds the stationary matrix polynomial corresponding to recursive Duffin "
"matrices.\n"
"More details in section 'Stability of matrices of polynomials' of paper "
"'Changes of variables in black-box estimation methods' published at "
"https://www.tol-project.org/export/HEAD/tolp/OfficialTolArchiveNetwork/"
"VariableChange/doc/Changes%20of%20variables%20in%20MCMC%20methods/"
"Changes%20of%20variables%20in%20MCMC%20methods.pdf",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BPolMatChgVarStationaryMatrixPolyn::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[Correlation] step 1"));
  DMat&  X = dMat(Arg(1));
  int degree = (int)Real(Arg(2));
  int dim = sqrt(double(X.Data().Size())/degree);
  ChgVarStationaryPolMat(X,dim,degree,contens_); 
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatChgVarStationaryPolMat2Vector);
DefMethod(1, BMatChgVarStationaryPolMat2Vector, "ChgVar.StationaryPolMat2Vector", 2, 2, 
"PolMatrix PolMatrix",
"(PolMatrix P, PolMatrix mask)",
""
"",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatChgVarStationaryPolMat2Vector::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[Correlation] step 1"));
  BPolMat&  P = PolMat(Arg(1));
  BPolMat&  mask = PolMat(Arg(2));
  ChgVarStationaryPolMat2Vector(P, mask, *(DMat*)(&contens_) );
}

//--------------------------------------------------------------------
DeclareContensClass(BPolMat, BPolMatTemporary, BPolMatChgVarStationaryVector2PolMat);
DefMethod(1, BPolMatChgVarStationaryVector2PolMat, "ChgVar.StationaryVector2PolMat", 2, 2, 
"Matrix PolMatrix",
"(Matrix x, PolMatrix mask)",
""
"",
BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BPolMatChgVarStationaryVector2PolMat::CalcContens()
//--------------------------------------------------------------------
{
//Std(BText("\nTRACE[Correlation] step 1"));
  DMat& x = dMat(Arg(1));
  BPolMat&  mask = PolMat(Arg(2));
  ChgVarStationaryVector2PolMat(x, mask, contens_);
}
/* */

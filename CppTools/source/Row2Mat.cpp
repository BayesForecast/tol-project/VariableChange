/* Row2Mat.cpp: Methods to convert from row to matrix

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#include "Row2Mat.h"
#include <tol/tol_matopr.hpp>


//--------------------------------------------------------------------
bool Row2DiagonalV(const DMat& X, int row, BVMat& D)
//--------------------------------------------------------------------
{
  int k = X.Columns();
  D.Eye(k);
  const double* x = X.Data().Buffer() + row*k;
  int i;
  for(i=0; i<k; i++, x++)
  {
    D.PutCell(i,i,*x);
  }
  return(true);
}

//--------------------------------------------------------------------
bool ReverseRow2DiagonalV(const DMat& X, int row, BVMat& D)
//--------------------------------------------------------------------
{
  int k = X.Columns();
  D.Eye(k);
  const double* x = X.Data().Buffer() + row*k;
  int i;
  for(i=k-1; i>=0; i--, x++)
  {
    D.PutCell(i,i,*x);
  }
  return(true);
}

//--------------------------------------------------------------------
bool Row2Square(const DMat& X, int row, DMat& M)
//--------------------------------------------------------------------
{
  int n = X.Columns();
  double k_ = sqrt(1.0*n);
  int i,j,k = (int)round(k_);
  bool ok = fabs(k-k_)<1E-5;
  if(!ok)
  {
    Error(BText("[Row2Square] Number of columns in 'x' (")+n+
      ") should be a square integer number k^2");
    return(false);
  }
  M.Alloc(k,k);
  M.SetAllValuesTo(0);
  const double* x = X.Data().Buffer() + row*n;
  for(i=0; i<k; i++)
  {
    for(j=0; j<k; j++, x++)
    {
      M(i,j) = *x;
    }
  }
  return(true);
}

//--------------------------------------------------------------------
bool Row2SquareV(const DMat& X, int row, BVMat& M)
//--------------------------------------------------------------------
{
  DMat m;
  Row2Square(X, row, m);
  M.DMat2dense(m);
  return(true);
}

//--------------------------------------------------------------------
bool Row2LowTriang(const DMat& X, int row, DMat& M)
//--------------------------------------------------------------------
{
  int n = X.Columns();
  double k_ = (-1.0+sqrt(1.0+8.0*n))/2.0;
  int i,j,k = (int)round(k_);
  bool ok = fabs(k-k_)<1E-5;
  if(!ok)
  {
    Error(BText("[Row2LowTriang] Number of columns in 'x' (")+n+
      ") should be a piramidal number k*(k-1)/2");
    return(false);
  }
  M.Alloc(k,k);
  M.SetAllValuesTo(0);
  const double* x = X.Data().Buffer() + row*n;
  for(i=0; i<k; i++)
  {
    for(j=0; j<=i; j++, x++)
    {
      M(i,j) = *x;
    }
  }
  return(true);
}

//--------------------------------------------------------------------
bool Row2LowTriangV(const DMat& X, int row, BVMat& M)
//--------------------------------------------------------------------
{
  DMat m;
  Row2LowTriang(X, row, m);
  M.DMat2sparse(m);
  return(true);
}

